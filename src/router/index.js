import { createRouter, createWebHistory } from "vue-router";
import sysRouter from "./sysRouter";
import { useMenuStore } from "../stores/useMenuStore";
import bookRouter from "./bookRouter";
import Layout from "@/layout/WebLayout.vue";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Layout",
      redirect: "/home",
      component: Layout,
      children: [...sysRouter, ...bookRouter],
    },

    {
      path: "/sys-login",
      component: () => import("../views/sys/sysLgoin.vue"),
    },
    {
      path: "/sso-login",
      component: () => import("../views/sso/ssoLogin.vue"),
    },
    {
      path: "/500",
      name: "500",
      component: () => import("../views/error/500.vue"),
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import('../views/AboutView.vue')
    // }
  ],
});

export default router;
router.beforeEach((to, from, next) => {
  const menuStore = useMenuStore();
  let tab = {
    name: to.name,
    path: to.path,
  };
  menuStore.addTabs(tab);
  menuStore.buldBreadcrumb(to);
  console.log(to)
  console.log("from", from);
  // console.log("next", next);
  // false;
  next();
});

