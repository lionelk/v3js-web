const bookRouter = [
  {
    path: "/book/xxx",
    name: "xxx",
    component: () => import("../views/book/xxx.vue"),
  },
  {
    path: "/book/yyy/yyy1",
    name: "yyy",
    component: () => import("../views/book/yyy/yyy1.vue"),
  },
];
export default bookRouter;
