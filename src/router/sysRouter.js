const sysRouter = [
  {
    path: "/home",
    name: "主页",
    component: () => import("../views/home.vue"),
  },
  {
    path: "/about",
    name: "关于",
    component: () => import("../views/about.vue"),
  },
  {
    path: "/sys/user",
    name: "用户管理",
    component: () => import("../views/sys/user.vue"),
  },
  {
    path: "/sys/role",
    name: "角色管理",
    component: () => import("../views/sys/role.vue"),
  },
  {
    path: "/sys/permissions",
    name: "权限管理",
    component: () => import("../views/sys/permissions.vue"),
  },
];

export default sysRouter;
