import { createApp } from "vue";
import { createPinia } from "pinia";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App.vue";
import router from "./router";

const app = createApp(App);
// 统一导入 element 图标
// import * as Icons from '@element-plus/icons'

// 注册全局 element-icons 组件
// Object.keys(Icons).forEach((key) => {
// console.log(key);
// app.component(key, Icons[key])
// })

app.use(createPinia());
app.use(router);
app.use(ElementPlus);

app.mount("#app");
