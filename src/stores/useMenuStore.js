import { defineStore } from "pinia";
import axios from "../utils/axios";

export const useMenuStore = defineStore({
  id: "menuStore",
  state: () => ({
    menuList: [],
    childMenuList: [],
    tabActivePath: "/home",
    tabs: [],
  }),
  getters: {
    getChildList(state) {
      return (index) => {
        console.log(state.menuList);
        let children = state.menuList[index].children;
        state.childMenuList = children;
        return children;
      };
    },
    // return state.menuList[index].children;
    childList(index) {
      return this.menuList[index];
    },
  },
  actions: {
    async menuTree() {
      await axios.post("/sys/menu/tree").then((res) => {
        if (res.state === 0) {
          this.menuList = res.data;
          let children = this.menuList[0].children;
          this.childMenuList = children;
        }
      });
    },
    addTabs(tab) {
      this.tabActivePath = tab.path;
      if (tab.path === "/home") {
        return;
      }
      if (this.tabs.length === 0) {
        this.tabs.push(tab);
      } else {
        let menu = this.tabs.filter((item) => {
          if (item.path === tab.path) {
            return item;
          }
        });
        if (menu.length === 0) {
          this.tabs.push(tab);
        }
      }
    },
    buldBreadcrumb(to) {
      if (this.menuList.length === 0) {
        this.menuTree()
      }

      console.log(to)
      console.log(this.menuList)

    }
  },
});
