import axios from "axios";
import qs from "qs";
import { ElMessageBox, ElLoading } from "element-plus";
import router from "../router";

axios.defaults.baseURL = "/api"; //正式
// axios.defaults.baseURL = "http://192.168.100.21:9527"; //测试
axios.defaults.headers.post["Access-Control-Allow-Origin"] = "*";
axios.defaults.headers.get["Access-Control-Allow-Origin"] = "*";
axios.defaults.headers.common["satoken"] = localStorage.getItem("satoken");

//加载效果
let loading;
const startLoading = () => {
  const options = {
    lock: true,
    text: "加载中...",
    background: "rgba(0,0,0,0.5)",
  };
  loading = ElLoading.service(options);
};

const endLoading = () => {
  loading.close();
};

//  让请求在浏览器中允许跨域携带cookie
axios.defaults.withCredentials = true;
//post请求头
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded;charset=UTF-8";
//设置超时
axios.defaults.timeout = 10000;
// axios.defaults.transformRequest = [
//   (data) => qs.stringify(data, { allowDots: true }),
// ];
// axios.defaults.paramsSerializer = (params) =>
//   qs.stringify(params, { allowDots: true });

axios.interceptors.request.use(
  (config) => {
    // config.transformRequest
    startLoading();
    return config;
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => {
    endLoading();
    if (response.status === 200) {
      return Promise.resolve(response);
    } else if (response.status === 401) {
      location.href = "sys-login?back=" + encodeURIComponent(location.href);
    } else {
      return Promise.reject(response);
    }
  },
  (error) => {
    endLoading();
    console.log(error);
    if (error.response.status >= 500) {
      console.log("跳转到 500 错误页");
      router.push("/500");
    }
    // error.response.data.message
    ElMessageBox.alert(error.response.statusText, "请求异常", {
      confirmButtonText: "确定",
    });
  }
);

export default {
  post(url, data) {
    return new Promise(function (resolve, reject) {
      axios({
        method: "post",
        url,
        data: qs.stringify(data),
      })
        .then((res) => {
          if (res) {
            resolve(res.data);
          }
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  },
  put(url, data) {
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url,
        data: qs.stringify(data),
      })
        .then((res) => {
          if (res) {
            resolve(res.data);
          }
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  },
  delete(url, data) {
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url,
        data: qs.stringify(data),
      })
        .then((res) => {
          if (res) {
            resolve(res.data);
          }
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  },

  get(url, data) {
    return new Promise((resolve, reject) => {
      axios({
        method: "get",
        url,
        params: qs.stringify(data),
      })
        .then((res) => {
          if (res === undefined) {
            resolve(res);
          } else {
            console.log(res);
            resolve(res.data);
          }
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  },
};
