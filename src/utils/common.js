export default {
  buildParam() {
    return "&client=PC&userType=sys-user";
  },

  sysMark() {
    return { userType: "sys-user", client: "PC" };
  },
};
