import axios from "../utils/axios";
import { reactive } from "vue";

export function menuList() {
  const menuList = reactive([]);
  axios.post("/sys/menu/tree").then((res) => {
    if (res.state === 0) {
      menuList.value = res.data;
    }
  });
  return menuList;
}
