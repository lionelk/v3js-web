import axios from "../utils/axios";
import common from "../utils/common";
import { ElMessage } from "element-plus";

export function isLogin() {
  let param = {
    userType: "sys-user",
    client: "PC",
  };
  axios.post("/sys/sso/isLogin", param).then((res) => {
    if (!res.data) {
      location.href =
        "sys-login?back=" +
        encodeURIComponent(location.href) +
        common.buildParam();
    } else {
      axios.get("/sys/getUserInfo", common.sysMark()).then((res) => {
        if (res.data.state === 0) {
          console.log(res);
        }
      });
    }
  });
}

export function auth() {
  let back = getParam("back", "/");
  let ticket = getParam("ticket");
  console.log(ticket);
  if (ticket) {
    doLoginByTicket(ticket);
  } else {
    goSsoAuthUrl();
  }

  // 重定向至认证中心
  function goSsoAuthUrl() {
    let baseInfo = {
      clientLoginUrl: location.href,
      client: "PC",
      type: "sys-user",
    };
    axios.post("/sys/sso/getSsoAuthUrl", baseInfo).then((res) => {
      console.log(res);
      location.href = res.data;
    });
  }

  // 根据ticket值登录
  function doLoginByTicket(ticket) {
    let baseInfo = {
      client: "PC",
      type: "sys-user",
      ticket: ticket,
    };
    axios.post("/sys/sso/doLoginByTicket", baseInfo).then((res) => {
      if (res.code === 200) {
        localStorage.setItem("satoken", res.data);
        location.href = decodeURIComponent(back);
      }
    });
  }

  // 从url中查询到指定名称的参数值
  function getParam(name, defaultValue) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      if (pair[0] === name) {
        return pair[1];
      }
    }
    return defaultValue === undefined ? null : defaultValue;
  }
}

export function beforLogin() {
  axios
    .post("/auth/sso/getRedirectUrl", {
      redirect: getParam("redirect", ""),
      mode: getParam("mode", ""),
      client: getParam("client", ""),
      userType: getParam("userType", ""),
    })
    .then((res) => {
      if (res.code === 200) {
        location.href = decodeURIComponent(res.data);
        ElMessage.success("登录成功");
      } else if (res.code === 401) {
        console.log("未登录");
      } else {
        //
        ElMessage.info("test");
      }
    });
}

export function doLogin(username, password) {
  let loginData = {
    client: getParam("client", ""),
    userType: getParam("userType", ""),
    name: username.value,
    pwd: password.value,
  };
  axios.post("/auth/sso/doLogin", loginData).then((res) => {
    console.log(res);
    if (res.state === 0) {
      localStorage.setItem("satoken", res.data);
      location.reload();
    } else {
      ElMessage.error(res.des);
    }
  });
}
function getParam(name, defaultValue) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  console.log(vars);
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] === name) {
      return pair[1] + (pair[2] ? "=" + pair[2] : "");
    }
  }
  return defaultValue === undefined ? null : defaultValue;
}
