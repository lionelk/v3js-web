import { fileURLToPath, URL } from "url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  // 配置跨域请求
  server: {
    // host: "0.0.0.0",
    port: 3000,
    proxy: {
      "/api": {
        target: "http://192.168.100.21:9527",
        // target: "http://localhost:9527",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
        // pathRewrite: {
        //   '^/api': ''
        // }
      },
    },
  },
});
